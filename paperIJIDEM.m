% Script para obtener la tabla de descriptores y una gráfica
% Sample Period = 10 ms
% Oxímetro versión 1.0.0

%Input parameters
transient_cut=50;

%Signal and journal
d=dir('data/ppg-device/000*.csv');
nFiles=length(d);
journal = readtable('data/ppg-device/journal.xlsx','sheet','Sheet1');

%Matrix inicialization
TS=cell(nFiles,1);
TS_filtered=cell(nFiles,1);
PIMa=cell(nFiles,1);
TauOpt=zeros(nFiles,1);
FNN=cell(nFiles,1);
embDim=zeros(nFiles,1);
meanPeriod=zeros(nFiles,1);
C_R=cell(nFiles,1);
R=cell(nFiles,1);

names=cell(nFiles,1);
asoGluco=zeros(nFiles,1);
hurstExp=zeros(nFiles,1);
larLyapunovExp=zeros(nFiles,1);
dimCorr=zeros(nFiles,1);

for i=1:nFiles
    names{i,1}=erase(d(i).name,'.csv');
end

%Chaotic descriptors
for i=1:nFiles
    asoGluco(i,1) = journal.Glucose(strcmp(journal.PPGID, names{i,1}));
    TS{i,1}=getTimeSeries(strcat('data/ppg-device/',d(i).name),2000);
    TS_filtered{i,1}=filterTimeSeries(TS{i,1},1,5,10,10);
    TS_filtered{i,1}=TS_filtered{i,1}(transient_cut+1:2000,1:1);
    disp(i);
    [PIMa{i,1},TauOpt(i,1)]=PIM(TS_filtered{i,1},80,200);
    [FNN{i,1},embDim(i,1)]=knn_deneme(TS_filtered{i,1},TauOpt(i,1),20,15,5);
    meanPeriod(i,1)=meanperiod(TS_filtered{i,1},30);
    larLyapunovExp(i,1)=lyarosenstein(TS_filtered{i,1},embDim(i,1),TauOpt(i,1),30,1/meanPeriod(i,1),600);
    [dimCorr(i,1), C_R{i,1}, R{i,1}]=corrdim(TS_filtered{i,1},embDim(i,1),TauOpt(i,1));
    hurstExp(i,1) = hurstexp(TS_filtered{i,1});
end

outputTable = table(asoGluco, dimCorr, hurstExp, larLyapunovExp,'RowNames',names);
writetable(outputTable,'outputTableOximeter.csv', 'WriteRowNames', true);