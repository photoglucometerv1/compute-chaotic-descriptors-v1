function mean_period = meanperiod(x,Fs)
% Estimates the mean period of the time series
% x:time series
% Fs:sampling frequency
% Based upon the Matlab example obtained from here: 
%http://www.mathworks.com/help/matlab/ref/fft.html
% Band-Pass filter example was obtained from
%http://www.mathworks.com/help/dsp/ref/fdesign.bandpass.html

T = 1/Fs;
L = length(x);
%t = (0:L-1)*T;

NFFT = 2^nextpow2(L); % Next power of 2 from length of y
X = fft(x,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);

% Plot single-sided amplitude spectrum.
%  plot(f,2*abs(X(1:NFFT/2+1))) 
%  title('Single-Sided Amplitude Spectrum of y(t)')
%  xlabel('Frequency (Hz)')
%  ylabel('|Y(f)|')

mean_period = 1/(sum(2*abs(X(1:NFFT/2+1)))/length(X(1:NFFT/2+1)));

end