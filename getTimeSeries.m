function timeSeries = getTimeSeries(fileName, length)
    fileId = fopen(fileName, 'r');
    file = textscan(fileId, '%f', length, 'Delimiter', ',');
    timeSeries = file{1};
end