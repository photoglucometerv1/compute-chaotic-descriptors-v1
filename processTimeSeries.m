function [hurstExp, maxLyapunovExp, correlationDim] = processTimeSeries(timeSeries)
    len = length(timeSeries);
    [AMI, optTau] = PIM(timeSeries, 80, 200);
    [FNN, embeddingDim] = knn_deneme(timeSeries, optTau, 20, 15, 5);
    meanPeriod = meanperiod(timeSeries, 30);
    
    hurstExp = hurstexp(timeSeries);
    maxLyapunovExp = lyarosenstein(timeSeries, embeddingDim, optTau, 30, 1 / meanPeriod, 600);
    correlationDim = corrdim(timeSeries, embeddingDim, optTau);
    
    figure(3);
    plot(timeSeries);
    legend('Time series');
    
    figure(4);
    plot3(timeSeries(1 + 2 * optTau : len), timeSeries(1 + optTau : len - optTau), timeSeries(1 : len - 2 * optTau));
    title('Attractor');
    xlabel('x(k+2\tau)');
    ylabel('x(k+\tau)');
    zlabel('x(k)');
end