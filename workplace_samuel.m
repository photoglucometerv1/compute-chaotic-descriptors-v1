%Filtros

%onemsFiltered = filterTimeSeries(filterTimeSeries(getTimeSeries("data/esp32-tests/1ms/test-1.csv",20000),1,5,10,1),4,5,10,1)
%tenmsFiltered = filterTimeSeries(filterTimeSeries(getTimeSeries("data/esp32-tests/1ms/test-1.csv",2000),1,5,10,10),4,5,10,10)

%para una sola señal

%parametros de entrada
% transient_cut=50;

%Leer la señal y tabla de glucosa
% TS=getTimeSeries('data/ppg-device/0001_20200518_1018_10_0.csv',2000);
% journal = readtable('data/ppg-device/journal.xlsx','sheet','Sheet1');

%Grafica: señal sin filtro
% figure;
% plot(TS);
% title('Unfiltered Signal');
% xlabel('Number of Sample');
% ylabel('Magnitude');

%Filtrar la señal
% TS_filtered=filterTimeSeries(TS,1,5,10,10);
% TS_filtered=TS_filtered(transient_cut+1:2000,1:1);

%Grafica: Señal filtrada
% figure;
% plot(TS_filtered);
% title('Filtered Signal');
% xlabel('Number of Sample');
% ylabel('Magnitude');

% asoGluco = journal.Glucose(strcmp(journal.PPGID, '0001_20200518_1018_10_0'));

% [PIMa,TauOpt]=PIM(TS_filtered,80,200);
% [FNN,embDim]=knn_deneme(TS_filtered,TauOpt,20,15,5);
% meanPeriod=meanperiod(TS_filtered,30);
% larLiapunovExp=lyarosenstein(TS_filtered,embDim,TauOpt,30,1/meanPeriod,600);
% [dimCorr, C_R, R]=corrdim(TS_filtered,embDim,TauOpt);
% hurstExp = hurstexp(TS_filtered);

%Grafica: atractor
% figure;
% plot3(TS_filtered(1+2*TauOpt:length(TS_filtered)),TS_filtered(1+TauOpt:length(TS_filtered)-TauOpt),TS_filtered(1:length(TS_filtered)-2*TauOpt));
% title('Attractor');
% xlabel('x(k+2\tau)');
% ylabel('x(k+\tau)');
% zlabel('x(k)');

%TS=getTimeSeries('data/ppg-device/0001_20200518_1018_10_0.csv',2000);
%TS=(TS-min(TS))/(max(TS)-min(TS))-0.325;
%TS=cumtrapz(TS);
%TS=(TS-min(TS))/(max(TS)-min(TS))
%plot(TS,'black');
%title('Integrated Signal');
%xlabel('Number of Sample');
%ylabel('Magnitude');

