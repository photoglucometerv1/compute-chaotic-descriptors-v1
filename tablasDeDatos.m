%Save the series values

%nFiles=length(d);
nFiles=2;
names=cell(nFiles,1);

for i=1:nFiles
    names{i,1}=erase(d(i).name,'.csv');
end

output = table(AsoGluco, dimCorr, hurstExp, larLiapunovExp,'RowNames',names);
writetable(output,'output.csv', 'WriteRowNames', true);

input=readtable('outputTables.csv', 'ReadRowNames', true);