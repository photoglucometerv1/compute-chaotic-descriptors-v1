attractorTable = readtable('outputTable.csv');
nFiles=height(attractorTable);

names = cell(nFiles,1);
asoGluco = zeros(nFiles,1);
hurstExp = zeros(nFiles,1);
larLiapunovExp = zeros(nFiles,1);
dimCorr = zeros(nFiles,1);
lowGlucoseDots=zeros(0,0);
mediumGlucoseDots=zeros(0,0);
highGlucoseDots=zeros(0,0);

names = attractorTable.Row(1:nFiles);
asoGluco = attractorTable.asoGluco(1:nFiles);
dimCorr = attractorTable.dimCorr(1:nFiles);
larLiapunovExp = attractorTable.larLiapunovExp(1:nFiles);
hurstExp = attractorTable.hurstExp(1:nFiles);


% figure;

for i=1:nFiles
    if asoGluco(i)<=95
        lowGlucoseDots=[lowGlucoseDots;[larLiapunovExp(i),hurstExp(i),dimCorr(i)]];
    else
        if asoGluco(i)<120
            mediumGlucoseDots=[mediumGlucoseDots;[larLiapunovExp(i),hurstExp(i),dimCorr(i)]];
        else
            highGlucoseDots=[highGlucoseDots;[larLiapunovExp(i),hurstExp(i),dimCorr(i)]];
        end
    end
        
end

figure;
hold on;
plot3(lowGlucoseDots(:,1),lowGlucoseDots(:,2),lowGlucoseDots(:,3),'.','MarkerSize',20,'LineStyle','none','Color',[0 6/7 0],'DisplayName','Normal or Low Glucose');
plot3(mediumGlucoseDots(:,1),mediumGlucoseDots(:,2),mediumGlucoseDots(:,3),'.','MarkerSize',20,'LineStyle','none','Color',[6/7 6/7 0],'DisplayName','Medium Glucose');
plot3(highGlucoseDots(:,1),highGlucoseDots(:,2),highGlucoseDots(:,3),'.','MarkerSize',20,'LineStyle','none','Color',[6/7 0 0],'DisplayName','High Glucose');
hold off;
title('Chaotic Descriptor Space');
xlabel('Lyapunov Exponent');
ylabel('Hurst Exponent');
zlabel('Correlation Dimension');
grid on;
legend;

view([0.2 0.21 0.23]);
% saveas(descriptorplotfig,'descriptorspace.fig');

