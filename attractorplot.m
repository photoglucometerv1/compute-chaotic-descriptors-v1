for i=1:nFiles
    figure;
    attractorplotfig=plot3(TS{i}(1+2*TauOpt(i):tamanoMuestra),TS{i}(1+TauOpt(i):tamanoMuestra-TauOpt(i)),TS{i}(1:tamanoMuestra-2*TauOpt(i)));
    title(strcat('Attractor',{' '},names(i)));
    xlabel('x(k+2\tau)');
    ylabel('x(k+\tau)');
    zlabel('x(k)');
    thisfilename=strcat('./fig/attractors/',names(i),'.fig');
    saveas(attractorplotfig,thisfilename{1});
end