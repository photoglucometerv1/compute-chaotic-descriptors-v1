function filteredTimeSeries = filterTimeSeries(timeSeries, n, Rp, Wp, Ts)
    %n -> orden
    %Rp -> ripple window [dB]
    %Wp -> frecuencia de corte [Hz]
    %Ts -> Periodo de muestreo [ms]
   
    Wn = Wp*2*Ts/1000;
    
    [b, a] = cheby2(n, Rp, Wn);
    %figure;
    %freqz(b, a);
    %title("Bode and Phase Diagrams");
    
    filteredTimeSeries = filter(b, a, timeSeries);
    %figure;
    %plot(timeSeries);
    %xlabel('Frequency [Hz]');
    %ylabel('|x[k]|');
    %title("unFiltered signal");

    %figure;
    %plot(filteredTimeSeries);
    %xlabel('Frequency [Hz]');
    %ylabel('|x[k]|');
    %title("Filtered signal");
end