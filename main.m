addpath('./funct/');

margenRuido=1;
tamanoMuestra=1800;

%Read the series' files
d=dir('series/*.csv');
nFiles=length(d);

TS=cell(nFiles,1);
PIMa=cell(nFiles,1);
TauOpt=zeros(nFiles,1);
FNN=cell(nFiles,1);
embDim=zeros(nFiles,1);
meanPeriod=zeros(nFiles,1);
C_R=cell(nFiles,1);
R=cell(nFiles,1);
names=cell(nFiles,1);

AsoGluco=zeros(nFiles,1);
hurstExp=zeros(nFiles,1);
larLyapunovExp=zeros(nFiles,1);
dimCorr=zeros(nFiles,1);

for i=1:nFiles
    names{i,1}=erase(d(i).name,'.csv');
end

for i=1:nFiles
    AsoGluco(i,1)=csvread(strcat('series/',d(i).name),0,0,[0,0,0,0]);
    TS{i,1}=csvread(strcat('series/',d(i).name),0,margenRuido,[0,margenRuido,0,margenRuido+tamanoMuestra]);
    disp(i);
    [PIMa{i,1},TauOpt(i,1)]=PIM(TS{i,1},80,200);
    [FNN{i,1},embDim(i,1)]=knn_deneme(TS{i,1},TauOpt(i,1),20,15,5);
    meanPeriod(i,1)=meanperiod(TS{i,1},30);
    larLyapunovExp(i,1)=lyarosenstein(TS{i,1},embDim(i,1),TauOpt(i,1),30,1/meanPeriod(i,1),600);
    [dimCorr(i,1), C_R{i,1}, R{i,1}]=corrdim(TS{i,1},embDim(i,1),TauOpt(i,1));
    hurstExp(i,1) = hurstexp(TS{i,1});
end

outputTable = table(asoGluco, dimCorr, hurstExp, larLyapunovExp,'RowNames',names);
writetable(outputTable,'outputTable.csv', 'WriteRowNames', true);
