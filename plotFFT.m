function plotFFT(Fs, timeSeries)
    %Fs -> frecuencia de sampleo
    L = length(timeSeries);
    Y = fft(timeSeries);
    
    P2 = abs(Y / L);
    P1 = P2(1 : L / 2 + 1);
    P1(2 : end - 1) = 2 * P1(2 : end - 1);
    f = Fs * (0 : (L / 2)) / L;
    
    figure(1);
    plot(f, P1);
    title("FFT");
end