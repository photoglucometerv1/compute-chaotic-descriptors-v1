function lle = lyarosenstein(x,m,tao,fs,meanperiod,maxiter) 
% d:divergence of nearest trajectoires
% x:signal
% tao:time delay
% m:embedding dimension
% fs:sampling frequency 

%Copyright (c) 2012, mirwais
%All rights reserved.
%Modifications: Walther Carballo Hernández
%Additional modifications: Samuel N. Huerta-Ruiz

N=length(x);
M=N-(m-1)*tao;
Y=psr_deneme(x,m,tao);
%Obtaining nearest distances
for i=1:M
    x0=ones(M,1)*Y(i,:);
    distance=sqrt(sum((Y-x0).^2,2));
    for j=1:M
        if abs(j-i)<=meanperiod
            distance(j)=1e10;
        end
    end
    [neardis(i) nearpos(i)]=min(distance);
end

%Obtaining log of divergence
for k=1:maxiter
    maxind=M-k;
    evolve=0;
    pnt=0;
    for j=1:M
        if j<=maxind && nearpos(j)<=maxind
            dist_k=sqrt(sum((Y(j+k,:)-Y(nearpos(j)+k,:)).^2,2));
             if dist_k~=0
                evolve=evolve+log(dist_k);
                pnt=pnt+1;
             end
        end
    end
    if pnt > 0
        d(k)=evolve/pnt;
    else
        d(k)=0;
    end
    
end


%% LLE Calculation
tlinear=20:90;
F = polyfit(tlinear,d(tlinear),1);
lle = F(1)*fs;

%% Plot results
% figure
% hold on;
% y = F(1)*[1:1:length(d)] + F(2);
% plot(y,'--')
% plot(d)
% title('Largest Lyapunov Exponent divergence');
% xlabel('Drive cycles');
% ylabel('LLE');
