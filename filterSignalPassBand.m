function XF = filterSignalPassBand(n, Rp, Wh, Wl, SampFreq, X)
    Wl = Wl * 2 / SampFreq;
    Wh = Wh * 2 / SampFreq;

    [b, a] = cheby2(n, Rp, [Wh Wl], 'bandpass');
    
    %figure(1);
    %freqz(b, a);
    %title("Frequencies");
    
    XF = filter(b, a, X);
    
    figure(1);
    plot(X);
    title("Original signal");
    
    figure(2);
    plot(XF);
    title("Filtered signal");
end